import React, { Component } from 'react';
import './App.css';
import Login from './component/Login/Login';
import { Menu } from './Menu/Menu';
class App extends Component {

  state = ({
    loggedIn: false,
    loginAs: ""
  })
  render() {
    return (
      <div>
        <Menu/>
        <div className="flex">
          <Login/>
        </div>
    </div>
    )
  }
}

export default App;
