import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
export class Login extends Component {

  state = ({
    username: "",
    password: "",
    admin: false,
    helperText: "",
    validated: false
  })

  styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
    dense: {
      marginTop: 16,
    },
    menu: {
      width: 200,
    },
  });

  render() {
    return (
      <div>
        <TextField
          required
          id="outlined-name"
          label="Username"
          className={this.styles.textField}
          placeholder="eg. john"
          margin="normal"
          variant="outlined"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          onChange={this.usernameCHandler}
        />
        {<
          TextField
          required
          id="outlined-password-input"
          label="Password"
          type="password"
          className={this.styles.textField}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
          margin="normal"
          variant="outlined"
          onChange={this.passcodeCHandler}
          disabled={!this.state.admin}
          helperText={this.state.helperText}
          error={this.state.validated}
          />}
      </div>
      
    )
  }

  usernameCHandler = evt => {
    this.setState({username: evt.target.value})
    if(evt.target.value.length >= 4){
      this.setState({admin: true, helperText: "Please enter 4-digit passcode", validated: true});
    }
    else {
      this.setState({admin: false, helperText: "", validated: false});
    }
  }

  passcodeCHandler = evt => {
    this.setState({password: evt.target.value})
  }

}

export default Login
