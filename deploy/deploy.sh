#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

./deploy/disableHostKeyChecking.sh

DEPLOY_SERVERS=$servers

ALL_SERVERS=(${servers//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  scp -i scp -r -i ~/.ssh/id_rsa build/ ubuntu@${server}:/deploy
  ssh ubuntu@${server} 'bash -s' < ./deploy/start-server.sh
done
